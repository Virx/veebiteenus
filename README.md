Projekti käivitamiseks peab arvutisse olema installitud mariaDB andmebaas ja Gradle. Andmebaasi andmed on failis 
```
#!python

application.properties
```
Arendamise töökeskkonnaks oli Intellij. Kindlasti peab olema installitud plugin Lombok. Teistes IDE'des pole proovinud projekti käima panna. Projekt käivitub käsuga 
```
#!python

gradle bootrun
```
WSDL saadaval aadressil [http://localhost:8090/ws/teams.wsdl](Link URL)
package ee.virx.team;


import ee.virx.player.Player;
import ee.virx.soap.generated.Sport;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;

import javax.persistence.*;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Getter
@Setter
public class Team {

    @Id
    @GeneratedValue
    private long id;
    @NotBlank
    private String name;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Sport sport;
    @OneToMany(fetch = FetchType.EAGER, cascade = CascadeType.ALL, orphanRemoval = true)
    @JoinColumn(name = "team", referencedColumnName = "id")
    private List<Player> players;

    public Team() {
        this.players = new ArrayList<>();
    }

    public Team(String name, Sport sport) {
        this();
        this.name = name;
        this.sport = sport;
    }

    public void addPlayer(Player player) {
        players.add(player);
    }
}

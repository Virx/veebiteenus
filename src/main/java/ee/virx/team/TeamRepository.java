package ee.virx.team;

import ee.virx.soap.generated.Sport;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface TeamRepository extends CrudRepository<Team, Long> {
    @Override
    List<Team> findAll();

    Team findOne(long id);

    List<Team> findAllBySport(Sport sport);

}

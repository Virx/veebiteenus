package ee.virx;

import org.springframework.boot.ApplicationRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

@SpringBootApplication
public class VeebiteenusApplication {

	public static void main(String[] args) {
		SpringApplication.run(VeebiteenusApplication.class, args);
	}
}

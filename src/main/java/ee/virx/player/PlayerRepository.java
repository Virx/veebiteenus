package ee.virx.player;

import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

import java.util.List;

@RepositoryRestResource
public interface PlayerRepository extends CrudRepository<Player, Long> {
    List<Player> findAll();
}

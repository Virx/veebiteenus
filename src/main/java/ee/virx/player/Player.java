package ee.virx.player;

import ee.virx.soap.generated.Color;
import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

@Entity
@Getter
@Setter
public class Player {

    @Id
    @GeneratedValue
    private long id;
    @NotBlank
    private String name;
    @Min(0)
    private int number;
    @NotNull
    @Enumerated(EnumType.STRING)
    private Color color;

    public Player() {}

    public Player(String name, int number, Color color) {
        this.name = name;
        this.number = number;
        this.color = color;
    }
}

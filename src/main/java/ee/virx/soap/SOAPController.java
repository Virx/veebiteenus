package ee.virx.soap;

import ee.virx.team.Team;
import ee.virx.team.TeamRepository;
import ee.virx.player.*;
import ee.virx.player.Player;
import ee.virx.soap.generated.*;
import org.springframework.ws.context.MessageContext;
import org.springframework.ws.server.endpoint.annotation.Endpoint;
import org.springframework.ws.server.endpoint.annotation.PayloadRoot;
import org.springframework.ws.server.endpoint.annotation.RequestPayload;
import org.springframework.ws.server.endpoint.annotation.ResponsePayload;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import javax.ws.rs.ProcessingException;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Endpoint
public class SOAPController {

    private static final String NAMESPACE_URI = "http://spring.io/guides/gs-producing-web-service";
    private List<String> idempotentList;

    private TeamRepository teamRepository;
    private PlayerRepository playerRepository;

    public SOAPController(TeamRepository teamRepository,
                          PlayerRepository playerRepository) {
        this.teamRepository = teamRepository;
        this.playerRepository = playerRepository;
        this.idempotentList = new ArrayList<>();
    }


    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getTeamRequest")
    @ResponsePayload
    public GetTeamResponse getTeam(@RequestPayload GetTeamRequest request, MessageContext message) {
        validate(request.getToken(), message);
        GetTeamResponse response = new GetTeamResponse();
        Team team = teamRepository.findOne(request.getId());
        response.setTeam(Converter.teamConverter(team));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getPlayerRequest")
    @ResponsePayload
    public GetPlayerResponse getPlayer(@RequestPayload GetPlayerRequest request, MessageContext message) {
        validate(request.getToken(), message);
        GetPlayerResponse response = new GetPlayerResponse();
        Player player = playerRepository.findOne(request.getId());
        response.setPlayer(Converter.playerConverter(player));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllTeamsRequest")
    @ResponsePayload
    public GetAllTeamsResponse getAllTeams(@RequestPayload GetAllTeamsRequest request, MessageContext message) {
        validate(request.getToken(), message);
        GetAllTeamsResponse response = new GetAllTeamsResponse();
        List<Team> teams;
        if (request.getSport() != null) {
            teams = teamRepository.findAllBySport(request.getSport());
        } else {
            teams = teamRepository.findAll();
        }
        teams.forEach(team -> response.getTeam().add(Converter.teamConverter(team)));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPlayersOfTeamRequest")
    @ResponsePayload
    public GetAllPlayersOfTeamResponse getAllPlayersofTeam(@RequestPayload GetAllPlayersOfTeamRequest request, MessageContext message) {
        validate(request.getToken(), message);
        GetAllPlayersOfTeamResponse response = new GetAllPlayersOfTeamResponse();
        List<Player> players = teamRepository.findOne(request.getTeamId()).getPlayers();
        if (!request.getName().isEmpty()) {
            players = players.stream().filter(player -> player.getName().equals(request.getName())).collect(Collectors.toList());
        }
        if (request.getNumber() != 0) {
            players = players.stream().filter(player -> player.getNumber() == request.getNumber()).collect(Collectors.toList());
        }
        if (request.getColor() != null) {
            players = players.stream().filter(player -> player.getColor() == request.getColor()).collect(Collectors.toList());
        }
        players.forEach(player -> response.getPlayer().add(Converter.playerConverter(player)));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addTeamRequest")
    @ResponsePayload
    public AddTeamResponse addTeam(@RequestPayload AddTeamRequest request, MessageContext message) {
        validate(request.getToken(), message);
        Team team = teamRepository.save(Converter.teamConverter(request));
        AddTeamResponse response = new AddTeamResponse();
        response.setTeam(Converter.teamConverter(team));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "addPlayerRequest")
    @ResponsePayload
    public AddPlayerResponse addPlayer(@RequestPayload AddPlayerRequest request, MessageContext message) {
        validate(request.getToken(), message);
        Team team = teamRepository.findOne(request.getTeamId());
        Player player = playerRepository.save(Converter.playerConverter(request));
        team.addPlayer(player);
        teamRepository.save(team);
        AddPlayerResponse response = new AddPlayerResponse();
        response.setPlayer(Converter.playerConverter(player));
        return response;
    }

    @PayloadRoot(namespace = NAMESPACE_URI, localPart = "getAllPlayersRequest")
    @ResponsePayload
    public GetAllPlayersResponse getAllPlayers(@RequestPayload GetAllPlayersRequest request, MessageContext message) {
        validate(request.getToken(), message);
        GetAllPlayersResponse response = new GetAllPlayersResponse();
        playerRepository.findAll().forEach(player -> response.getPlayer().add(Converter.playerConverter(player)));
        return response;
    }

    private void validate(String token, MessageContext messageContext) {
        if (authCheck(messageContext) && idempotentCheck(token)) {
            throw new ProcessingException("authorization or idempontent check failed");
        } else {
            idempotentList.add(token);
        }
    }

    private boolean authCheck(MessageContext message) {
        SaajSoapMessage headerRequest = (SaajSoapMessage) message.getRequest();
        return headerRequest.getSaajMessage().getMimeHeaders().getHeader("auth").length > 0;
    }

    private boolean idempotentCheck(String token) {
        return idempotentList.stream()
                .filter(s -> s.equals(token))
                .collect(Collectors.toList()).size() > 0;
    }

}


package ee.virx.soap.generated;

import javax.xml.bind.annotation.XmlRegistry;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ee.virx.soap.generated package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {


    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ee.virx.soap.generated
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link GetTeamResponse }
     * 
     */
    public GetTeamResponse createGetTeamResponse() {
        return new GetTeamResponse();
    }

    /**
     * Create an instance of {@link Team }
     * 
     */
    public Team createTeam() {
        return new Team();
    }

    /**
     * Create an instance of {@link AddPlayerRequest }
     * 
     */
    public AddPlayerRequest createAddPlayerRequest() {
        return new AddPlayerRequest();
    }

    /**
     * Create an instance of {@link GetAllPlayersOfTeamRequest }
     * 
     */
    public GetAllPlayersOfTeamRequest createGetAllPlayersOfTeamRequest() {
        return new GetAllPlayersOfTeamRequest();
    }

    /**
     * Create an instance of {@link GetAllTeamsRequest }
     * 
     */
    public GetAllTeamsRequest createGetAllTeamsRequest() {
        return new GetAllTeamsRequest();
    }

    /**
     * Create an instance of {@link GetAllTeamsResponse }
     * 
     */
    public GetAllTeamsResponse createGetAllTeamsResponse() {
        return new GetAllTeamsResponse();
    }

    /**
     * Create an instance of {@link GetAllPlayersRequest }
     * 
     */
    public GetAllPlayersRequest createGetAllPlayersRequest() {
        return new GetAllPlayersRequest();
    }

    /**
     * Create an instance of {@link AddTeamRequest }
     * 
     */
    public AddTeamRequest createAddTeamRequest() {
        return new AddTeamRequest();
    }

    /**
     * Create an instance of {@link GetPlayerResponse }
     * 
     */
    public GetPlayerResponse createGetPlayerResponse() {
        return new GetPlayerResponse();
    }

    /**
     * Create an instance of {@link Player }
     * 
     */
    public Player createPlayer() {
        return new Player();
    }

    /**
     * Create an instance of {@link AddPlayerResponse }
     * 
     */
    public AddPlayerResponse createAddPlayerResponse() {
        return new AddPlayerResponse();
    }

    /**
     * Create an instance of {@link GetAllPlayersOfTeamResponse }
     * 
     */
    public GetAllPlayersOfTeamResponse createGetAllPlayersOfTeamResponse() {
        return new GetAllPlayersOfTeamResponse();
    }

    /**
     * Create an instance of {@link GetPlayerRequest }
     * 
     */
    public GetPlayerRequest createGetPlayerRequest() {
        return new GetPlayerRequest();
    }

    /**
     * Create an instance of {@link GetTeamRequest }
     * 
     */
    public GetTeamRequest createGetTeamRequest() {
        return new GetTeamRequest();
    }

    /**
     * Create an instance of {@link GetAllPlayersResponse }
     * 
     */
    public GetAllPlayersResponse createGetAllPlayersResponse() {
        return new GetAllPlayersResponse();
    }

    /**
     * Create an instance of {@link AddTeamResponse }
     * 
     */
    public AddTeamResponse createAddTeamResponse() {
        return new AddTeamResponse();
    }

}


package ee.virx.soap.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for sport.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="sport">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="FOOTBALL"/>
 *     &lt;enumeration value="VOLLEYBALL"/>
 *     &lt;enumeration value="BASKETBALL"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "sport", namespace = "http://spring.io/guides/gs-producing-web-service")
@XmlEnum
public enum Sport {

    FOOTBALL,
    VOLLEYBALL,
    BASKETBALL;

    public String value() {
        return name();
    }

    public static Sport fromValue(String v) {
        return valueOf(v);
    }

}

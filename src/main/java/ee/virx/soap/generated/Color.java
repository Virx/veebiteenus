
package ee.virx.soap.generated;

import javax.xml.bind.annotation.XmlEnum;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Java class for color.
 * 
 * <p>The following schema fragment specifies the expected content contained within this class.
 * <p>
 * <pre>
 * &lt;simpleType name="color">
 *   &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *     &lt;enumeration value="BLACK"/>
 *     &lt;enumeration value="WHITE"/>
 *     &lt;enumeration value="RED"/>
 *     &lt;enumeration value="GREEN"/>
 *     &lt;enumeration value="BLUE"/>
 *     &lt;enumeration value="YELLOW"/>
 *     &lt;enumeration value="GRAY"/>
 *     &lt;enumeration value="ORANGE"/>
 *   &lt;/restriction>
 * &lt;/simpleType>
 * </pre>
 * 
 */
@XmlType(name = "color", namespace = "http://spring.io/guides/gs-producing-web-service")
@XmlEnum
public enum Color {

    BLACK,
    WHITE,
    RED,
    GREEN,
    BLUE,
    YELLOW,
    GRAY,
    ORANGE;

    public String value() {
        return name();
    }

    public static Color fromValue(String v) {
        return valueOf(v);
    }

}

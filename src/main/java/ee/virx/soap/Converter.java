package ee.virx.soap;


import ee.virx.soap.generated.AddPlayerRequest;
import ee.virx.soap.generated.AddTeamRequest;
import ee.virx.soap.generated.Player;
import ee.virx.soap.generated.Team;

import java.math.BigInteger;

public class Converter {

    public static Team teamConverter(ee.virx.team.Team team) {
        Team soapTeam = new Team();
        soapTeam.setId(team.getId());
        soapTeam.setSport(team.getSport());
        soapTeam.setName(team.getName());
        team.getPlayers().forEach(player -> soapTeam.getPlayer().add(playerConverter(player)));
        return soapTeam;
    }

    public static Player playerConverter(ee.virx.player.Player player) {
        Player soapPlayer = new Player();
        soapPlayer.setId(player.getId());
        soapPlayer.setName(player.getName());
        soapPlayer.setNumber(new BigInteger(player.getNumber() + ""));
        soapPlayer.setColor(player.getColor());
        return soapPlayer;
    }

    public static ee.virx.player.Player playerConverter(AddPlayerRequest player) {
        return new ee.virx.player.Player(player.getName(), player.getNumber(), player.getColor());
    }

    public static ee.virx.team.Team teamConverter(AddTeamRequest team) {
        return new ee.virx.team.Team(team.getName(), team.getSport());
    }
}

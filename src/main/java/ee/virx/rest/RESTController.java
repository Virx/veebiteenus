package ee.virx.rest;

import ee.virx.team.Team;
import ee.virx.team.TeamRepository;
import ee.virx.player.Player;
import ee.virx.player.PlayerRepository;
import ee.virx.soap.generated.Color;
import ee.virx.soap.generated.Sport;
import lombok.AllArgsConstructor;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@AllArgsConstructor
public class RESTController {

    private PlayerRepository playerRepository;
    private TeamRepository teamRepository;

    @RequestMapping(value = "/team/all", method = RequestMethod.GET)
    public List<Team> getTeams(@RequestHeader(value="auth", required = true) String auth,
                               @RequestParam(required = false) Sport sport) {
        if (sport != null) {
            return teamRepository.findAllBySport(sport);
        }
        return teamRepository.findAll();
    }

    @RequestMapping(value = "/team/{id}", method = RequestMethod.GET)
    public Team getTeam(@RequestHeader(value="auth", required = true) String auth,
                        @PathVariable(value = "id") long id) {
        return teamRepository.findOne(id);
    }

    @RequestMapping(value = "/team", method = RequestMethod.POST)
    public Team addTeam(@RequestHeader(value="auth", required = true) String auth,
                        @RequestBody Team team) {
        return teamRepository.save(team);
    }

    @RequestMapping(value = "player/{id}", method = RequestMethod.GET)
    public Player getPlayer(@RequestHeader(value="auth", required = true) String auth,
                            @PathVariable(value = "id") long id) {
        return playerRepository.findOne(id);
    }

    @RequestMapping(value = "/team/{id}/player", method = RequestMethod.POST)
    public Team addPlayer(@RequestHeader(value="auth", required = true) String auth,
                          @RequestBody Player player,
                          @PathVariable(value = "id") long id) {
        Team team = teamRepository.findOne(id);
        team.addPlayer(player);
        teamRepository.save(team);
        return team;
    }

    @RequestMapping(value = "/team/{id}/player", method = RequestMethod.GET)
    public List<Player> getAllPlayers(@RequestHeader(value="auth", required = true) String auth,
                                      @PathVariable(value = "id") long id,
                                      @RequestParam(required=false) String name,
                                      @RequestParam(required=false) Integer number,
                                      @RequestParam(required=false) Color color) {
        return findAllPlayers(name, number, color, id);
    }

    @RequestMapping(value = "/player/all", method = RequestMethod.GET)
    public List<Player> getAllPlayers() {
        return playerRepository.findAll();
    }

    public List<Player> findAllPlayers(String name, Integer number, Color color, long teamId) {
        List<Player> players = teamRepository.findOne(teamId).getPlayers();
        if (name != null) {
            players = players.stream().filter(player -> player.getName().equals(name)).collect(Collectors.toList());
        }
        if (number != null) {
            players = players.stream().filter(player -> player.getNumber() == number).collect(Collectors.toList());
        }
        if (color != null) {
            players = players.stream().filter(player -> player.getColor() == color).collect(Collectors.toList());
        }
        return players;
    }
}

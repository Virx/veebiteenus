package ee.virx;


import ee.virx.team.Team;
import ee.virx.team.TeamRepository;
import ee.virx.player.Player;
import ee.virx.soap.generated.Color;
import ee.virx.soap.generated.Sport;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.stereotype.Component;

@Component
@EnableScheduling
public class Bootstrap implements ApplicationRunner {

    @Autowired
    TeamRepository teamRepository;

    @Override
    public void run(ApplicationArguments args) throws Exception {
        Team team1 = new Team("FC Chelsea", Sport.FOOTBALL);
        Team team2 = new Team("FC Barcelona", Sport.FOOTBALL);
        Team team3 = new Team("Võru VK", Sport.VOLLEYBALL);
        Team team4 = new Team("Kalev Cramo", Sport.BASKETBALL);

        team1.addPlayer(new Player("Sulev", 1, Color.RED));
        team1.addPlayer(new Player("Kalev", 2, Color.GREEN));
        team1.addPlayer(new Player("Madis", 6, Color.GREEN));
        team1.addPlayer(new Player("Jaan", 12, Color.BLACK));

        team2.addPlayer(new Player("Kerli", 1, Color.BLUE));
        team2.addPlayer(new Player("Laura", 4, Color.BLUE));
        team2.addPlayer(new Player("Liis", 14, Color.GRAY));

        team3.addPlayer(new Player("Lible", 14, Color.ORANGE));
        team3.addPlayer(new Player("Arno", 12, Color.WHITE));
        team3.addPlayer(new Player("Toots", 11, Color.GRAY));
        team3.addPlayer(new Player("Kiir", 4, Color.GRAY));

        team4.addPlayer(new Player("Tõnisson", 2, Color.YELLOW));
        team4.addPlayer(new Player("Imelik", 8, Color.YELLOW));
        team4.addPlayer(new Player("Lesta", 5, Color.YELLOW));

        teamRepository.save(team1);
        teamRepository.save(team2);
        teamRepository.save(team3);
        teamRepository.save(team4);
    }

}
